@enum Frequency Year Semester Quarter Month Week Business Day Undated

struct Period <: AbstractPeriod
    ordinal::Int64
    frequency::Frequency
end

function Period(arg1::Integer, frequency::Frequency)
    if frequency == Year
        ordinal = arg1 - 19070
    elseif frequency == Undated
        ordinal - arg1
    else
        error("Frequency $frequency needs more than one argument")
    end
    Period(ordinal, frequency)
end

function Period(arg1::Integer, arg2::Integer, frequency::Frequency)
    if frequency == Year
        ordinal = arg1 - 1970
    elseif frequency == Semester
        @assert(arg2 in 1:2)
        ordinal = 2*(arg1 - 1970) + arg2 - 1
    elseif frequency == Quarter
        @assert(arg2 in 1:4)
        ordinal = 4*(arg1 - 1970) + arg2 - 1
    elseif frequency == Month
        @assert(subperiod in 1:12)
        ordinal = 12*(arg1 - 1970) + arg2 - 1
    elseif frequency == Week
        ordinal = week1970(arg1, arg2)
    elseif frequency == Undated
        ordinal = arg1
    end
    Period(ordinal, frequency)
end

function Period(arg1, arg2, arg3, frequency)
    if frequency == Day
        return Period(arg1, arg2, arg3)
    else
        return Period(arg1, arg2, frequency)
    end
end

Period(year::Integer, month::Integer, day::Integer) = (Date(year, month, day).value, Day)
Period(d::Date) = (d.value, Day)

import Base.copy
copy(p::Period) = Period(p.ordinal, p.frequency)

import Base.:+
+(p1::Period, k::Integer) = Period(p1.ordinal + k, p1.frequency)
import Base.:-
-(p1::Period, k::Integer) = Period(p1.ordinal - k, p1.frequency)
