# Periods package

The Periods package provide a compact representation of time periods used in statistics. A Period is represented by the frequency and an ordinal with 0 for the period containing 1970-01-01.
Using an ordinal integer simplifies period arithmetic. Main functionalities contains parsing of strings into Periods and formatting of Periods in user controlled representations.
No support is provided for infra day time periods.

## Frequencies

The following frequencies are supported: year, semester, quarter, month, week (ISO starting on Monday), day, undated

 - undated periods are represented by -Inf < Integer < Inf. We need 0 and negative integer to always be able to define a period before the current one.
 
Implemented as an ``enum`` ?

## Objects

### Period
    - ordinal::Integer
    - frequency::Frequency

### ExtendedPeriod

- ordinal::Integer
    - frequency::Frequency
    - offset::Integer
    - offset_frequency::Frequency
    - multiple::Integer

We want to be able to represent a week starting on Wednesday (frequency=week, offset=2, offset_frequency=Day) or periods of two quarters starting in February (month, offset = 1, offset_frequency=month, multiple=2)

## Constructors
 -  Period(arg1::Integer, frequency)
 -  Period(arg1::Integer, arg2::Integer, frequency)
 -  Period(year::Integer, arg2::Integer, arg3::Integer, frequency)
 -  ExtendedPeriod(year::Integer, subperiod::Integer, frequency, [offset=offset::Integer, offset_frequency=offset_frequency::Frequency, multiple=multiple::Integer])

More constructors are needed

## Shortcuts
 - year(year::Integer)
 - semester(year::Integer, semester::[1,2])
 - quarter(year::Integer, quarter::[1:4])
 - month(year::Integer, month::[1:12])
 - week(year::Integer, week::[1:53])
 - day(year::Integer, month::Integer, day::Integer)

## Period ranges
```
Period(2010, 3, :Month):(Period(2011,12, :Month) must be equivalent to a vector of Periods. Frequency must be the same at both ends.


## Parsing and formatting
 - parsing and formating of period strings is implemented as an extension of the mechanism used for dates in Julia Standard Library. Some default formats will be provided. Be careful about ambiguous representations.

### Default format
 - "2010": year
 - "2012-S2": semester
 - "2013-Q3": quarter
 - "2013-02": month
 - "2014-02-29": day
 - "2014-W53": week

For ExtendedPeriod display, the offset is ignored; only the basic period is represented; it is the first period of the multiple 

## Integer arithmetic must be supported
 - ::Period - ::Period = ::Integer (1. frequency must be the same, 2. no plans to introduce duration)
 - ::Period + ::Integer = :: ::Period
 - ::Period - ::Integer = :: ::Period

